#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

int VERBOSE = 0;

const char *get_all_digits() { return "0123456789abcdefghijklmnopqrstuvwxyz"; }
const size_t ALL_DIGIT_COUNT = 36;

void arithmatoy_free(char *number) { free(number); }

char *arithmatoy_add(unsigned int base, const char *lhs, const char *rhs) 
{
  if (VERBOSE) 
    fprintf(stderr, "add: entering function\n");

  if (VERBOSE) 
    fprintf(stderr, "base: %d | left : %s | right : %s\n", base, lhs, rhs);

  if (base < 2 || base > 36)
  {
    printf("La base est en dehors de l'intervalle");
    return NULL;
  }

  int lhs_length = strlen(lhs);
  int rhs_length = strlen(rhs);
  int max_length = lhs_length > rhs_length ? lhs_length : rhs_length;
  int carry = 0;

  char *lhs_reversed = strdup(lhs);
  char *rhs_reversed = strdup(rhs);

  reverse(lhs_reversed);
  reverse(rhs_reversed);

  // Allocates memory for the result with the maximum length of the result + 1 for the potential carry, + 1 for the null char at the end.
  char *result = malloc(max_length + 2);

  for (int i = 0; i < max_length; i++) 
  {
      size_t lhs_digit = i < lhs_length ? get_digit_value(lhs_reversed[i]) : 0;
      size_t rhs_digit = i < rhs_length ? get_digit_value(rhs_reversed[i]) : 0;
      int sum = lhs_digit + rhs_digit + carry;
      carry = sum / base;
      int result_digit = sum % base;

      if (VERBOSE) 
        fprintf(stderr,"left : %d | right : %d, carry : %d\n", base, lhs_digit, rhs_digit, carry);

      result[i] = to_digit(result_digit);
  }

  if (carry > 0) 
  {
      result[max_length] = to_digit(carry);
      max_length++;
  }

  result[max_length] = '\0';

  char *final_result = strdup(result);

  reverse(final_result);
  const char *final_result_no_leading_zeros = drop_leading_zeros(final_result);
  char *result_no_leading_zeros = strdup(final_result_no_leading_zeros);
  
  free(lhs_reversed);
  free(rhs_reversed);
  free(result);
  free(final_result);
  
  return result_no_leading_zeros;
}

char *arithmatoy_sub(unsigned int base, const char *lhs, const char *rhs) 
{
  if (VERBOSE) 
  {
    fprintf(stderr, "sub: entering function\n");
  }
  if (VERBOSE)
  {
    fprintf(stderr, "base: %d | left : %s | right : %s\n", base, lhs, rhs);
  }
  // Fill the function, the goal is to compute lhs - rhs (assuming lhs > rhs)
  // You should allocate a new char* large enough to store the result as a
  // string Implement the algorithm Return the result

  if (base < 2 || base > 36)
  {
    fprintf(stderr, "La base est en dehors de l'intervalle");
    return NULL;
  }

  int lhs_length = strlen(lhs);
  int rhs_length = strlen(rhs);
  int max_length = lhs_length > rhs_length ? lhs_length : rhs_length;
  int borrow = 0;

  char *lhs_reversed = strdup(lhs);
  char *rhs_reversed = strdup(rhs);

  reverse(lhs_reversed);
  reverse(rhs_reversed);

  char *result = malloc(max_length + 2); 

  for (int i = 0; i < max_length; i++) 
  {
    int lhs_digit = i < lhs_length ? get_digit_value(lhs_reversed[i]) : 0;
    int rhs_digit = i < rhs_length ? get_digit_value(rhs_reversed[i]) : 0;
    int sub = lhs_digit - rhs_digit - borrow;

    if (sub < 0) 
    {
      sub = sub + base;
      borrow = 1;
    } else {
      borrow = 0;
    }

    if (VERBOSE)
      fprintf(stderr, "base : %d | left : %d | right : %d\n | borrow : %d\n", base, lhs_digit, rhs_digit, borrow);

    result[i] = to_digit(sub);
  }

  // Si la retenue est toujours à 1 après avoir traité tous les nombres, c'est qu'on a un résultat négatif
  if (borrow == 1) 
  {
    free(lhs_reversed);
    free(rhs_reversed);
    free(result);
    return NULL;
  }
  result[max_length] = '\0';

  char *final_result = strdup(result);

  reverse(final_result);

  // On retire les leading zeros
  const char *final_result_no_leading_zeros = drop_leading_zeros(final_result);
  char *result_no_leading_zeros = strdup(final_result_no_leading_zeros);
  
  free(lhs_reversed);
  free(rhs_reversed);
  free(result);
  free(final_result);
  
  return result_no_leading_zeros;
}

char *arithmatoy_mul(unsigned int base, const char *lhs, const char *rhs) 
{
    if (VERBOSE) {
    fprintf(stderr, "mul: entering function\n");
  }

  if (base < 2 || base > 36)
  {
    fprintf(stderr, "La base est en dehors de l'intervalle");
    return NULL ;
  }

  int lhs_length = strlen(lhs);
  int rhs_length = strlen(rhs);

  char *lhs_reversed = strdup(lhs);
  char *rhs_reversed = strdup(rhs);

  reverse(lhs_reversed);
  reverse(rhs_reversed);

  char *result = malloc(lhs_length + rhs_length + 2); 
  memset(result, '0', lhs_length + rhs_length); 

  // On multiplie chacun des digit de rhs avec chacun de ceux de lhs et on ajoute le résultat à la bonne position dans le tableau de résultats.
  for (int i = 0; i < lhs_length; i++) 
  {
    int carry = 0;
    for (int j = 0; j < rhs_length; j++) 
    {
      int lhs_digit = get_digit_value(lhs_reversed[i]);
      int rhs_digit = get_digit_value(rhs_reversed[j]);

      int temp = get_digit_value(result[i+j]) + lhs_digit * rhs_digit + carry;
      result[i + j] = to_digit(temp % base);
      carry = temp / base;
    }
    // On ajoute la retenue si elle reste à la fin
    if (carry > 0)
      result[i + rhs_length] = to_digit(get_digit_value(result[i + rhs_length]) + carry);
  }

  result[lhs_length + rhs_length] = '\0';

  char *final_result = strdup(result);
  reverse(final_result);
  
  const char *final_result_no_leading_zeros = drop_leading_zeros(final_result);
  char *result_no_leading_zeros = strdup(final_result_no_leading_zeros);

  free(lhs_reversed);
  free(rhs_reversed);
  free(result);
  free(final_result);

  return result_no_leading_zeros;
}

// Here are some utility functions that might be helpful to implement add, sub
// and mul:

unsigned int get_digit_value(char digit) {
  // Convert a digit from get_all_digits() to its integer value
  if (digit >= '0' && digit <= '9') {
    return digit - '0';
  }
  if (digit >= 'a' && digit <= 'z') {
    return 10 + (digit - 'a');
  }
  return -1;
}

char to_digit(unsigned int value) {
  // Convert an integer value to a digit from get_all_digits()
  if (value >= ALL_DIGIT_COUNT) {
    debug_abort("Invalid value for to_digit()");
    return 0;
  }
  return get_all_digits()[value];
}

char *reverse(char *str) {
  // Reverse a string in place, return the pointer for convenience
  // Might be helpful if you fill your char* buffer from left to right
  const size_t length = strlen(str);
  const size_t bound = length / 2;
  for (size_t i = 0; i < bound; ++i) {
    char tmp = str[i];
    const size_t mirror = length - i - 1;
    str[i] = str[mirror];
    str[mirror] = tmp;
  }
  return str;
}

const char *drop_leading_zeros(const char *number) {
  // If the number has leading zeros, return a pointer past these zeros
  // Might be helpful to avoid computing a result with leading zeros
  if (*number == '\0') {
    return number;
  }
  while (*number == '0') {
    ++number;
  }
  if (*number == '\0') {
    --number;
  }
  return number;
}

void debug_abort(const char *debug_msg) {
  // Print a message and exit
  fprintf(stderr, debug_msg);
  exit(EXIT_FAILURE);
}

