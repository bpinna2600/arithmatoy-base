# Aucun n'import ne doit être fait dans ce fichier


def nombre_entier(n: int) -> str:
    if (n == 0):
            return "0"
    
    result = ""

    for x in range(n):
        result += "S"
    
    result += "0"

    return result


def S(n: str) -> str:
    if n == "0":
        return "S0"

    result = "S" 
    result += n

    return result

def addition(a: str, b: str) -> str:
    
    int_a = 0;
    int_b = 0

    for c in a:
        if c == "S":
            int_a += 1

    for c in b:
        if c == "S":
            int_b += 1

    return nombre_entier(int_a + int_b)


def multiplication(a: str, b: str) -> str:
    if a == "0" or b == "0":
        return "0"

    int_a = 0;
    int_b = 0

    for c in a:
        if c == "S":
            int_a += 1

    for c in b:
        if c == "S":
            int_b += 1

    return nombre_entier(int_a * int_b)



def facto_ite(n):
    result = 1
    for i in range(1, n + 1):
        result *= i
    return result


def facto_rec(n):
    if n == 0:
        return 1
    else:
        return n * facto_rec(n - 1)


def fibo_rec(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibo_rec(n - 1) + fibo_rec(n - 2)


def fibo_ite(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        a, b = 0, 1
        for _ in range(n - 1):
            a, b = b, a + b
        return b


def golden_phi(n):
    return fibo_ite(n + 1) / fibo_ite(n)


def sqrt5(n):
    return (golden_phi(n) ** 2 + 1) / 2

def pow(x, n):
    return x ** n


